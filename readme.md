# BSF Golang MSA Example 


### prepare-project-setting 

##### window 개발 환경 
* chocolaty 
* vscode + remote container 로 로컬 개발 

###### linux 개발 환경 택일 
* vscode + remote container 
* vscode 개발 

## 폴더 구조 

```sh 
# 폴더의 용도 설명 
├── bin
├── pkg
├── scripts
│   ├── databases              # project database 폴더 
│   │   └── tidb              # tidb init sql 파일용 폴더 
│   ├── example                # 예제 스크립트 폴더 
│   └── test                   # test script 폴더 
│   ├── ci                     # ci 용 폴더 
│   │   ├── docker          
│   │   └── helm
└── service                     # go kit micorservice app root 
    └── order                   # go kit domain root 
        ├── cmd                 # go kit domain 실행 가능한 어플리케이션 
        ├── implementation      # service 구현체 
        ├── middleware          # go kit middleware 
        ├── tidb                # repository
        └── transport           # go kit transport 
            └── http            # go kit transport http 구현체 폴더 
```

### VS Code Defined Task 

* Ctrl + Shift + B : Run local server, 로컬 호스트 18080 으로 http server 을 실행 
* Ctrl * Shift + P > select task 정의된 Task 를 실행 : `.vscode/tasks.json` 참조 
  * Build - Order
  * Run - Local - Order (default task) 
  * Reqeust - Order
  * Push remote VCS - Git




# 참고자료 
https://github.com/go-kit/kit.git
https://github.com/shijuvar/gokit-examples.git
