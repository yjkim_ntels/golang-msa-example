# create tables 
create table orders (
    id int(11) NOT NULL AUTO_INCREMENT,
    customerid char(120) ,
    useyn char(2),
    createon char(120) ,
    restaurantid char(120) ,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin

CREATE TABLE orderitems (
    id int(11) NOT NULL AUTO_INCREMENT ,
    order_id int(11) NOT NULL,
    product_code char(120) NOT NULL DEFAULT '',
    name char(120) NOT NULL DEFAULT '',
    unitprice char(120) NOT NULL DEFAULT '',
    quantity char(120) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`,`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin

# insert 
INSERT INTO orders (customerid,useyn, createOn, restaurantid)VALUES(1,'Y',1,1)

INSERT INTO orderitems (order_id,product_code, name, unitprice, quantity)VALUES (1,1,1,1,1)

# select 
select id, customerid, createon, restaurantid from orders where id = 1;

select * from orderitems;

# update 
update orders
set useyn = "N"
where id = 1

