#!/bin/bash 

cd ../../ 

# init go project 
go mod init github.com/ntels/bsf

# create dir
mkdir -p service/order/cmd
mkdir -p service/order/implementation
mkdir -p service/order/middleware    
mkdir -p service/order/tidb      
mkdir -p service/order/transport/http

# create main app
cat << EOF | tee service/order/cmd/main.go
package main

func main() {
}
EOF

# create business interface 
cat << EOF | tee service/order/service.go
package order

import (
	"context"
)

type Service interface {
}
EOF

# create order entity interface 
cat << EOF | tee service/order/order.go
package order

import "context"

type Order struct {
	ID           string      \`json:"id,omitempty"\`
	CustomerID   string      \`json:"customer_id"\`
	UseYn        string      \`json:"use_yn"\`
	CreatedOn    int64       \`json:"created_on,omitempty"\`
	RestaurantID string      \`json:"restaurant_id"\`
	OrderItems   []OrderItem \`json:"order_items,omitempty"\`
}

// OrderItem represents items in an order
type OrderItem struct {
	ProductCode string  \`json:"product_code"\`
	Name        string  \`json:"name"\`
	UnitPrice   float32 \`json:"unit_price"\`
	Quantity    int32   \`json:"quantity"\`
}


type Repository interface {
}

EOF

# add logger main app 

cat << EOF | tee service/order/cmd/main.go
package main

import (
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

func main() {

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

   	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

}
EOF

go run service/order/cmd/main.go 