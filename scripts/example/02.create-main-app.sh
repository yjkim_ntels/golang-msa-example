#!/bin/bash 

cd ../../ 

# add logger main app 
cat << EOF | tee service/order/cmd/main.go
package main

import (
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

func main() {

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

   	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

}
EOF

go run service/order/cmd/main.go 


# add datasource main app 

cat << EOF | tee service/order/cmd/main.go
package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

func main() {

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

   	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	var db *sql.DB
	{
		var err error
		db, err = sql.Open("mysql",	"root@tcp(192.168.5.86:4000)/test")
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
        
   		//for test
		db.Ping()
	}

}
EOF

go run service/order/cmd/main.go 
