#!/bin/bash 

cd ../../ 

# create tidb repository
cat << END_OF_COMMANDS | tee service/order/tidb/repository.go
package tidb

import (
	"database/sql"
	"errors"

	"github.com/go-kit/kit/log"

	"github.com/ntels/bsf/service/order"
)

var (
	ErrRepository = errors.New("unable to handle request")
)

type repository struct {
	db     *sql.DB
	logger log.Logger
}

func New(db *sql.DB, logger log.Logger) (order.Repository, error) {
	return &repository{
		db:     db,
		logger: log.With(logger, "rep", "tidb"),
	}, nil
}
END_OF_COMMANDS

# create service interface 
cat << END_OF_COMMANDS | tee service/order/service.go
package order

import (
	"context"
	"errors"
)

var (
	ErrOrderNotFound   = errors.New("order not found")
	ErrCmdRepository   = errors.New("unable to command repository")
	ErrQueryRepository = errors.New("unable to query repository")
)

type Service interface {
}
END_OF_COMMANDS


# create service method 
cat << END_OF_COMMANDS | tee service/order/implementation/service.go 
package implementation

import (
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	ordersvc "github.com/ntels/bsf/service/order"
)

type service struct {
	repository ordersvc.Repository
	logger     log.Logger
}

func NewService(rep ordersvc.Repository, logger log.Logger) ordersvc.Service {
	return &service{
		repository: rep,
		logger:     logger,
	}
}
END_OF_COMMANDS


# service/order.go "context" imported but not used 빼고는 나머지 클래스 문제 없는 상황 

