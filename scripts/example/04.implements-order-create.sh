#!/bin/bash 

cd ../../ 

# add business logic "order create"

## add "order create" method to service/order/order.go 
cat << END_OF_COMMANDS | tee service/order/order.go
package order

import "context"

type Order struct {
	ID           string      \`json:"id,omitempty"\`
	CustomerID   string      \`json:"customer_id"\`
	UseYn        string      \`json:"use_yn"\`
	CreatedOn    int64       \`json:"created_on,omitempty"\`
	RestaurantID string      \`json:"restaurant_id"\`
	OrderItems   []OrderItem \`json:"order_items,omitempty"\`
}

// OrderItem represents items in an order
type OrderItem struct {
	ProductCode string  \`json:"product_code"\`
	Name        string  \`json:"name"\`
	UnitPrice   float32 \`json:"unit_price"\`
	Quantity    int32   \`json:"quantity"\`
}

type Repository interface {
	CreateOrder(ctx context.Context, order Order) (Order, error)
}

END_OF_COMMANDS

## add "order create" method to service/order/tidb/repository.go 
cat << END_OF_COMMANDS | tee service/order/tidb/repository.go
package tidb

import (
	"context"
	"database/sql"
	"errors"
	"strconv"

	"github.com/go-kit/kit/log"

	"github.com/ntels/bsf/service/order"
)

var (
	ErrRepository = errors.New("unable to handle request")
)

type repository struct {
	db     *sql.DB
	logger log.Logger
}

func New(db *sql.DB, logger log.Logger) (order.Repository, error) {
	return &repository{
		db:     db,
		logger: log.With(logger, "rep", "tidb"),
	}, nil
}

// add this method 
func (repo *repository) CreateOrder(ctx context.Context, order order.Order) (order.Order, error) {
	//logger := log.With(repo.logger, "method", "Create")
	//level.Info(logger).Log("log test in repo ", "value")

	tx, err := repo.db.Begin()
	if err != nil {
		return order, err
	}
	defer tx.Commit()

	{
		orderStmt, err := tx.Prepare(\`
			INSERT INTO orders (customerid,useyn,createOn, restaurantid)
			  VALUES(?,?,?,?)
		\`)
		if err != nil {
			tx.Rollback()
			return order, err
		}
		defer orderStmt.Close()

		res, err := orderStmt.Exec(order.CustomerID, order.UseYn, order.CreatedOn, order.RestaurantID)
		if err != nil {
			tx.Rollback()
			return order, err
		}

		id, _ := res.LastInsertId()
		order.ID = strconv.FormatInt(id, 10)

		itemStmt, err := tx.Prepare(\`
				INSERT INTO orderitems (order_id,product_code, name, unitprice, quantity)
					VALUES (?,?,?,?,?)
			\`)
		if err != nil {
			tx.Rollback()
			return order, err
		}
		defer itemStmt.Close()

		for _, v := range order.OrderItems {
			if _, err := itemStmt.Exec(order.ID, v.ProductCode, v.Name, v.UnitPrice, v.Quantity); err != nil {
				tx.Rollback()
				return order, err
			}
		}

	}

	return order, tx.Commit()
}


func (repo *repository) Close() error {
	return repo.db.Close()
}
END_OF_COMMANDS

## add "order create" method to service/order/implementation/service.go 
cat << END_OF_COMMANDS | tee service/order/implementation/service.go 
package implementation

import (
	"context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	ordersvc "github.com/ntels/bsf/service/order"
)

type service struct {
	repository ordersvc.Repository
	logger     log.Logger
}

func NewService(rep ordersvc.Repository, logger log.Logger) ordersvc.Service {
	return &service{
		repository: rep,
		logger:     logger,
	}
}

// add this method "order create"
func (s *service) Create(ctx context.Context, order ordersvc.Order) (string, error) {
	logger := log.With(s.logger, "method", "Create")

	//invoke repository method
	res, err := s.repository.CreateOrder(ctx, order)
	if err != nil {
		level.Error(logger).Log("err", err)
		return "", ordersvc.ErrCmdRepository
	}

	return res.ID, nil
}
END_OF_COMMANDS

## add "order create" method to service/order/service.go 
cat << END_OF_COMMANDS | tee service/order/service.go
package order

import (
	"context"
	"errors"
)

var (
	ErrOrderNotFound   = errors.New("order not found")
	ErrCmdRepository   = errors.New("unable to command repository")
	ErrQueryRepository = errors.New("unable to query repository")
)

type Service interface {
	Create(ctx context.Context, order Order) (string, error)    // add this method "order create"
}
END_OF_COMMANDS

## add "order create" bussiness logic to main.go 
cat << END_OF_COMMANDS | tee service/order/cmd/main.go
package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"github.com/ntels/bsf/service/order"
	"github.com/ntels/bsf/service/order/tidb"

	ordersvc "github.com/ntels/bsf/service/order/implementation"
)

func main() {

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	var db *sql.DB
	{
		var err error
		// Connect to the "ordersdb" database
		db, err = sql.Open("mysql",
			"root@tcp(192.168.10.73:32561)/test")
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}

		//for test
		//db.Ping()
	}

	var svc order.Service
	{

		repository, err := tidb.New(db, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = ordersvc.NewService(repository, logger)

		svc.Create(nil, order.Order{})

	}

}
END_OF_COMMANDS


go run service/order/cmd/main.go

## output 
# level=info svc=order ts=2019-11-28T07:55:52.453667633Z caller=main.go:31 msg="service started"                                ## main 에서 시작 
# level=info svc=order ts=2019-11-28T07:55:52.453744648Z caller=service.go:28 method=Create loggerinservice:=value              ## service 를 경유하여 로깅 출력 
# level=info svc=order ts=2019-11-28T07:55:52.453761834Z caller=repository.go:33 rep=tidb method=Create logtestinrepo=value     ## repo class 를 경유하여 로깅 출력 확인 
# level=info svc=order ts=2019-11-28T07:55:52.45377737Z caller=main.go:63 msg="service ended"                                   ## main 에서 종료 완료 
