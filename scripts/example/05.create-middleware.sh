#!/bin/bash 

cd ../../ 

# define logging middleware 
cat << END_OF_COMMANDS | tee service/order/middleware/service.go
package middleware

import "github.com/ntels/bsf/service/order"

// Middleware describes a service middleware.
type Middleware func(service order.Service) order.Service

END_OF_COMMANDS

# implement logging middleware 
cat << END_OF_COMMANDS | tee service/order/middleware/logging.go

package middleware

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"

	"github.com/ntels/bsf/service/order"
)

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next order.Service) order.Service {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   order.Service
	logger log.Logger
}

func (mw loggingMiddleware) Create(ctx context.Context, order order.Order) (id string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "Create", "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.Create(ctx, order)
}

END_OF_COMMANDS

# add implement logging middle ware 
cat << END_OF_COMMANDS | tee service/order/cmd/main.go
package main

import (
	"database/sql"
	"os"

	_ "github.com/go-sql-driver/mysql"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"github.com/ntels/bsf/service/order"
	"github.com/ntels/bsf/service/order/middleware"
	"github.com/ntels/bsf/service/order/tidb"

	ordersvc "github.com/ntels/bsf/service/order/implementation"
)

func main() {

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	var db *sql.DB
	{
		var err error
		// Connect to the "ordersdb" database
		db, err = sql.Open("mysql",
			"root@tcp(192.168.10.73:32561)/test")
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}

		//for test
		//db.Ping()
	}

	var svc order.Service
	{

		repository, err := tidb.New(db, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = ordersvc.NewService(repository, logger)

		// Add service middleware here
		// Logging middlewarego run service/order/cmd/main.go
		svc = middleware.LoggingMiddleware(logger)(svc)

	}

}
END_OF_COMMANDS

go run service/order/cmd/main.go

## output 
# level=info svc=order ts=2019-11-28T08:55:23.153204583Z caller=main.go:33 msg="service started"
# level=info svc=order ts=2019-11-28T08:55:23.153261578Z caller=main.go:67 msg="service ended"
