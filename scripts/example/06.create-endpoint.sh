#!/bin/bash 

cd ../../ 

# define and implement endpoints 
cat << END_OF_COMMANDS | tee service/order/transport/endpoint.go
package transport

import (
	"context"

	"github.com/go-kit/kit/endpoint"

	"github.com/ntels/bsf/service/order"
)

// Endpoints holds all Go kit endpoints for the Order service.
type Endpoints struct {
	Create       endpoint.Endpoint
}

// MakeEndpoints initializes all Go kit endpoints for the Order service.
func MakeEndpoints(s order.Service) Endpoints {
	return Endpoints{
		Create:       makeCreateEndpoint(s),
	}
}

func makeCreateEndpoint(s order.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest) // type assertion
		id, err := s.Create(ctx, req.Order)
		return CreateResponse{ID: id, Err: err}, nil
	}
}
END_OF_COMMANDS

# implement request response model 
cat << END_OF_COMMANDS | tee service/order/transport/request_response.go
package transport

import (
	"github.com/ntels/bsf/service/order"
)

// CreateRequest holds the request parameters for the Create method.
type CreateRequest struct {
	Order order.Order
}

// CreateResponse holds the response values for the Create method.
type CreateResponse struct {
	ID  string `json:"id"`
	Err error  `json:"error,omitempty"`
}
END_OF_COMMANDS

# implement http transport 
cat << END_OF_COMMANDS | tee service/order/transport/http/service.go 
package http

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/ntels/bsf/service/order"
	"net/http"

	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	"github.com/ntels/bsf/service/order/transport"
)

var (
	ErrBadRouting = errors.New("bad routing")
)

// NewService wires Go kit endpoints to the HTTP transport.
func NewService(
	svcEndpoints transport.Endpoints, options []kithttp.ServerOption, logger log.Logger,
) http.Handler {

	// set-up router and initialize http endpoints
	var (
		r            = mux.NewRouter()
		errorLogger  = kithttp.ServerErrorLogger(logger)
		errorEncoder = kithttp.ServerErrorEncoder(encodeErrorResponse)
	)
	options = append(options, errorLogger, errorEncoder)

    r.Methods("POST").Path("/orders").Handler(kithttp.NewServer(
		svcEndpoints.Create,
		decodeCreateRequest,
		encodeResponse,
		options...,
	))
	return r
}

func decodeCreateRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req transport.CreateRequest
	if e := json.NewDecoder(r.Body).Decode(&req.Order); e != nil {
		return nil, e
	}
	return req, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeErrorResponse(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

func encodeErrorResponse(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case order.ErrOrderNotFound:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
END_OF_COMMANDS

# append implement transport to main app 
cat << END_OF_COMMANDS | tee service/order/cmd/main.go
package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kithttp "github.com/go-kit/kit/transport/http"
	_ "github.com/lib/pq"

	"github.com/ntels/bsf/service/order"
	ordersvc "github.com/ntels/bsf/service/order/implementation"
	"github.com/ntels/bsf/service/order/middleware"
	"github.com/ntels/bsf/service/order/tidb"
	"github.com/ntels/bsf/service/order/transport"
	httptransport "github.com/ntels/bsf/service/order/transport/http"

)

func main() {
	var (
		httpAddr = flag.String("http.addr", ":8080", "HTTP listen address")
	)
	flag.Parse()
	// initialize our OpenCensus configuration and defer a clean-up

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	var db *sql.DB
	{
		var err error
		// Connect to the "ordersdb" database
		db, err = sql.Open("mysql",
			"root@tcp(192.168.10.73:32561)/test")
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
	}

	// Create Order Service
	var svc order.Service
	{

		repository, err := tidb.New(db, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = ordersvc.NewService(repository, logger)
		// Add service middleware here
		// Logging middleware
		svc = middleware.LoggingMiddleware(logger)(svc)

	}
	// Create Go kit endpoints for the Order Service
	// Then decorates with endpoint middlewares
	var endpoints transport.Endpoints
	{
		endpoints = transport.MakeEndpoints(svc)
	
	}


	var h http.Handler
	{
        serverOptions := []kithttp.ServerOption{}
		h = httptransport.NewService(endpoints, serverOptions, logger)
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		level.Info(logger).Log("transport", "HTTP", "addr", *httpAddr)
		server := &http.Server{
			Addr:    *httpAddr,
			Handler: h,
		}
		errs <- server.ListenAndServe()
	}()

	level.Error(logger).Log("exit", <-errs)
}
END_OF_COMMANDS

go run service/order/cmd/main.go


## test 
: '
 curl \
 -H "Content-Type: application/json" \
 -d '{"customer_id": "superman", "use_yn" : "Y", "created_on":123,"restaurant_id":"super rest", "order_items":[{"product_code":"1","name":"test1","unit_price":100,"quantity":3},{"product_code":"2","name":"test2","unit_price":50,"quantity":3}]}' \
 localhost:8080/orders -v
'
## log 
# level=info svc=order ts=2019-12-03T07:18:32.511594913Z caller=main.go:46 msg="service started"
# level=info svc=order ts=2019-12-03T07:18:32.511768679Z caller=main.go:99 transport=HTTP addr=:8080
# svc=order ts=2019-12-03T07:18:54.552416943Z caller=logging.go:29 method=Create took=160.263633ms err=null
## db query
#mysql> select * from orders; select * from orderitems;
#+----+------------+-------+----------+--------------+
#| id | customerid | useyn | createon | restaurantid |
#+----+------------+-------+----------+--------------+
#|  1 | superman   | Y     | 123      | super rest   |
#+----+------------+-------+----------+--------------+
#1 row in set (0.00 sec)

#+----+----------+--------------+-------+-----------+----------+
#| id | order_id | product_code | name  | unitprice | quantity |
#+----+----------+--------------+-------+-----------+----------+
#|  1 |        1 | 1            | test1 | 100       | 3        |
#|  3 |        1 | 2            | test2 | 50        | 3        |
#+----+----------+--------------+-------+-----------+----------+
#2 rows in set (0.00 sec)








