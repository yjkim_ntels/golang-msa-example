#!/bin/bash 

cd ../../ 

#└── order
#    ├── cmd
#    │   └── main.go
#    ├── implementation
#    │   └── service.go
#    ├── middleware
#    │   ├── logging.go
#    │   └── service.go
#    ├── order.go
#    ├── service.go
#    ├── tidb
#    │   └── repository.go
#    └── transport
#        ├── endpoint.go
#        ├── http
#        │   └── service.go
#        └── request_response.go

cat << END_OF_COMMANDS | tee service/order/cmd/main.go

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/go-sql-driver/mysql"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kithttp "github.com/go-kit/kit/transport/http"
	_ "github.com/lib/pq"

	"github.com/ntels/bsf/service/order"
	ordersvc "github.com/ntels/bsf/service/order/implementation"
	"github.com/ntels/bsf/service/order/middleware"
	"github.com/ntels/bsf/service/order/tidb"
	"github.com/ntels/bsf/service/order/transport"
	httptransport "github.com/ntels/bsf/service/order/transport/http"
)

func main() {
	var (
		httpAddr = flag.String("http.addr", ":8080", "HTTP listen address")
	)
	flag.Parse()
	// initialize our OpenCensus configuration and defer a clean-up

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	var db *sql.DB
	{
		var err error
		// Connect to the "ordersdb" database
		db, err = sql.Open("mysql",
			"root@tcp(192.168.10.73:32561)/test")
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		db.Ping()

	}

	// Create Order Service
	var svc order.Service
	{

		repository, err := tidb.New(db, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = ordersvc.NewService(repository, logger)
		// Add service middleware here
		// Logging middleware
		svc = middleware.LoggingMiddleware(logger)(svc)

	}
	// Create Go kit endpoints for the Order Service
	// Then decorates with endpoint middlewares
	var endpoints transport.Endpoints
	{
		endpoints = transport.MakeEndpoints(svc)
	}

	var h http.Handler
	{

		serverOptions := []kithttp.ServerOption{}
		h = httptransport.NewService(endpoints, serverOptions, logger)
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		level.Info(logger).Log("transport", "HTTP", "addr", *httpAddr)
		server := &http.Server{
			Addr:    *httpAddr,
			Handler: h,
		}
		errs <- server.ListenAndServe()
	}()

	level.Error(logger).Log("exit", <-errs)
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/implementation/service.go
package implementation

import (
	"context"
	"database/sql"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	ordersvc "github.com/ntels/bsf/service/order"
)

type service struct {
	repository ordersvc.Repository
	logger     log.Logger
}

func NewService(rep ordersvc.Repository, logger log.Logger) ordersvc.Service {
	return &service{
		repository: rep,
		logger:     logger,
	}
}

// add this method "order create"
func (s *service) Create(ctx context.Context, order ordersvc.Order) (string, error) {
	logger := log.With(s.logger, "method", "Create")

	//invoke repository method
	res, err := s.repository.CreateOrder(ctx, order)
	if err != nil {
		level.Error(logger).Log("err", err)
		return "", ordersvc.ErrCmdRepository
	}

	return res.ID, nil
}

// GetByID returns an order given by id
func (s *service) GetByID(ctx context.Context, id string) (ordersvc.Order, error) {
	logger := log.With(s.logger, "method", "GetByID")
	order, err := s.repository.GetOrderByID(ctx, id)
	if err != nil {
		level.Error(logger).Log("err", err)
		if err == sql.ErrNoRows {
			return order, ordersvc.ErrOrderNotFound
		}
		return order, ordersvc.ErrQueryRepository
	}
	return order, nil
}

// ChangeStatus changes the status of an order
func (s *service) ChangeStatus(ctx context.Context, id string, status string) error {
	logger := log.With(s.logger, "method", "ChangeStatus")
	if err := s.repository.ChangeOrderStatus(ctx, id, status); err != nil {
		level.Error(logger).Log("err", err)
		return ordersvc.ErrCmdRepository
	}
	return nil
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/middleware/logging.go

package middleware

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"

	"github.com/ntels/bsf/service/order"
)

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next order.Service) order.Service {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   order.Service
	logger log.Logger
}

func (mw loggingMiddleware) Create(ctx context.Context, order order.Order) (id string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "Create", "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.Create(ctx, order)
}

func (mw loggingMiddleware) GetByID(ctx context.Context, id string) (order order.Order, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetByID", "OrderID", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetByID(ctx, id)
}

func (mw loggingMiddleware) ChangeStatus(ctx context.Context, id string, status string) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "ChangeStatus", "OrderID", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.ChangeStatus(ctx, id, status)
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/middleware/service.go
package middleware

import "github.com/ntels/bsf/service/order"

// Middleware describes a service middleware.
type Middleware func(service order.Service) order.Service

END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/order.go
package order

import "context"

type Order struct {
	ID           string      \`json:"id,omitempty"\`
	CustomerID   string      \`json:"customer_id"\`
	UseYn        string      \`json:"use_yn"\`
	CreatedOn    int64       \`json:"created_on,omitempty"\`
	RestaurantID string      \`json:"restaurant_id"\`
	OrderItems   []OrderItem \`json:"order_items,omitempty"\`
}

// OrderItem represents items in an order
type OrderItem struct {
	ProductCode string  \`json:"product_code"\`
	Name        string  \`json:"name"\`
	UnitPrice   float32 \`json:"unit_price"\`
	Quantity    int32   \`json:"quantity"\`
}

type Repository interface {
	CreateOrder(ctx context.Context, order Order) (Order, error)
	GetOrderByID(ctx context.Context, id string) (Order, error)
	ChangeOrderStatus(ctx context.Context, id string, status string) error
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/service.go
package order

import (
	"context"
	"errors"
)

var (
	ErrOrderNotFound   = errors.New("order not found")
	ErrCmdRepository   = errors.New("unable to command repository")
	ErrQueryRepository = errors.New("unable to query repository")
)

type Service interface {
	Create(ctx context.Context, order Order) (string, error)    // add this method "order create"
	GetByID(ctx context.Context, id string) (Order, error)
	ChangeStatus(ctx context.Context, id string, status string) error
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/tidb/repository.go
package tidb

import (
	"context"
	"database/sql"
	"errors"
	"strconv"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"github.com/ntels/bsf/service/order"
)

var (
	ErrRepository = errors.New("unable to handle request")
)

type repository struct {
	db     *sql.DB
	logger log.Logger
}

func New(db *sql.DB, logger log.Logger) (order.Repository, error) {
	return &repository{
		db:     db,
		logger: log.With(logger, "repository", "tidb"),
	}, nil
}

func (repo *repository) CreateOrder(ctx context.Context, order order.Order) (order.Order, error) {
	//logger := log.With(repo.logger, "method", "Create")
	//level.Info(logger).Log("log test in repo ", "value")

	tx, err := repo.db.Begin()
	if err != nil {
		return order, err
	}
	defer tx.Commit()

	{
		orderStmt, err := tx.Prepare(\`
			INSERT INTO orders (customerid,useyn,createOn, restaurantid)
			  VALUES(?,?,?,?)
		\`)
		if err != nil {
			tx.Rollback()
			return order, err
		}
		defer orderStmt.Close()

		res, err := orderStmt.Exec(order.CustomerID, order.UseYn, order.CreatedOn, order.RestaurantID)
		if err != nil {
			tx.Rollback()
			return order, err
		}

		id, _ := res.LastInsertId()
		order.ID = strconv.FormatInt(id, 10)

		itemStmt, err := tx.Prepare(\`
				INSERT INTO orderitems (order_id,product_code, name, unitprice, quantity)
					VALUES (?,?,?,?,?)
			\`)
		if err != nil {
			tx.Rollback()
			return order, err
		}
		defer itemStmt.Close()

		for _, v := range order.OrderItems {
			if _, err := itemStmt.Exec(order.ID, v.ProductCode, v.Name, v.UnitPrice, v.Quantity); err != nil {
				tx.Rollback()
				return order, err
			}
		}

	}

	return order, tx.Commit()
}

// ChangeOrderStatus changes the order status
func (repo *repository) ChangeOrderStatus(ctx context.Context, orderId string, status string) error {
	sql := \`
UPDATE orders
SET status=$2
WHERE id=$1 \`

	_, err := repo.db.ExecContext(ctx, sql, orderId, status)
	if err != nil {
		return err
	}
	return nil
}

// GetOrderByID query the order by given id
func (repo *repository) GetOrderByID(ctx context.Context, id string) (order.Order, error) {
	var orderRow = order.Order{}
	if err := repo.db.QueryRowContext(ctx,
		"SELECT id, customerid, createon, restaurantid FROM orders WHERE id = ?",
		id).
		Scan(
			&orderRow.ID, &orderRow.CustomerID, &orderRow.CreatedOn, &orderRow.RestaurantID,
		); err != nil {
		level.Error(repo.logger).Log("err", err.Error())
		return orderRow, err
	}
	// ToDo: Query order items from orderitems table
	return orderRow, nil
}

func (repo *repository) Close() error {
	return repo.db.Close()
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/transport/endpoint.go
package transport

import (
	"context"

	"github.com/go-kit/kit/endpoint"

	"github.com/ntels/bsf/service/order"
)

// Endpoints holds all Go kit endpoints for the Order service.
type Endpoints struct {
	Create       endpoint.Endpoint
	GetByID      endpoint.Endpoint
	ChangeStatus endpoint.Endpoint
}

// MakeEndpoints initializes all Go kit endpoints for the Order service.
func MakeEndpoints(s order.Service) Endpoints {
	return Endpoints{
		Create:       makeCreateEndpoint(s),
		GetByID:      makeGetByIDEndpoint(s),
		ChangeStatus: makeChangeStatusEndpoint(s),
	}
}

func makeCreateEndpoint(s order.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest) // type assertion
		id, err := s.Create(ctx, req.Order)
		return CreateResponse{ID: id, Err: err}, nil
	}
}

func makeGetByIDEndpoint(s order.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetByIDRequest)
		orderRes, err := s.GetByID(ctx, req.ID)
		return GetByIDResponse{Order: orderRes, Err: err}, nil
	}
}

func makeChangeStatusEndpoint(s order.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ChangeStatusRequest)
		err := s.ChangeStatus(ctx, req.ID, req.Status)
		return ChangeStatusResponse{Err: err}, nil
	}
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/transport/request_response.go
package transport

import (
	"github.com/ntels/bsf/service/order"
)

// CreateRequest holds the request parameters for the Create method.
type CreateRequest struct {
	Order order.Order
}

// CreateResponse holds the response values for the Create method.
type CreateResponse struct {
	ID  string 
	Err error  
}

// GetByIDRequest holds the request parameters for the GetByID method.
type GetByIDRequest struct {
	ID string
}

// GetByIDResponse holds the response values for the GetByID method.
type GetByIDResponse struct {
	Order order.Order \`json:"order"\`
	Err   error       \`json:"error,omitempty"\`
}

// ChangeStatusRequest holds the request parameters for the ChangeStatus method.
type ChangeStatusRequest struct {
	ID     string \`json:"id"\`
	Status string \`json:"status"\`
}

// ChangeStatusResponse holds the response values for the ChangeStatus method.
type ChangeStatusResponse struct {
	Err error \`json:"error,omitempty"\`
}
END_OF_COMMANDS

cat << END_OF_COMMANDS | tee service/order/transport/http/service.go
package http

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/ntels/bsf/service/order"
	"net/http"

	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	"github.com/ntels/bsf/service/order/transport"
)

var (
	ErrBadRouting = errors.New("bad routing")
)

// NewService wires Go kit endpoints to the HTTP transport.
func NewService(
	svcEndpoints transport.Endpoints, options []kithttp.ServerOption, logger log.Logger,
) http.Handler {

	// set-up router and initialize http endpoints
	var (
		r            = mux.NewRouter()
		errorLogger  = kithttp.ServerErrorLogger(logger)
		errorEncoder = kithttp.ServerErrorEncoder(encodeErrorResponse)
	)
	options = append(options, errorLogger, errorEncoder)

	r.Methods("POST").Path("/orders").Handler(kithttp.NewServer(
		svcEndpoints.Create,
		decodeCreateRequest,
		encodeResponse,
		options...,
	))
	// HTTP Post - /orders/{id}
	r.Methods("GET").Path("/orders/{id}").Handler(kithttp.NewServer(
		svcEndpoints.GetByID,
		decodeGetByIDRequest,
		encodeResponse,
		options...,
	))

	// HTTP Post - /orders/status
	r.Methods("POST").Path("/orders/status").Handler(kithttp.NewServer(
		svcEndpoints.ChangeStatus,
		decodeChangeStausRequest,
		encodeResponse,
		options...,
	))

	return r
}

func decodeCreateRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req transport.CreateRequest
	if e := json.NewDecoder(r.Body).Decode(&req.Order); e != nil {
		return nil, e
	}
	return req, nil
}

func decodeGetByIDRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	return transport.GetByIDRequest{ID: id}, nil
}

func decodeChangeStausRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req transport.ChangeStatusRequest
	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, e
	}
	return req, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeErrorResponse(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

func encodeErrorResponse(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case order.ErrOrderNotFound:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
END_OF_COMMANDS



