package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kithttp "github.com/go-kit/kit/transport/http"
	_ "github.com/lib/pq"

	"github.com/ntels/bsf/service/order"
	ordersvc "github.com/ntels/bsf/service/order/implementation"
	"github.com/ntels/bsf/service/order/middleware"
	"github.com/ntels/bsf/service/order/tidb"
	"github.com/ntels/bsf/service/order/transport"
	httptransport "github.com/ntels/bsf/service/order/transport/http"

	ot "github.com/go-kit/kit/tracing/opentracing"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

func main() {
	var (
		httpAddr = flag.String("http.addr", ":18080", "HTTP listen address")
	)
	flag.Parse()

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", "order",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("msg", "logging  service started")
	defer level.Info(logger).Log("msg", "service ended")

	cfg := config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
		},
	}
	tracer, closer, err := cfg.New(
		"BSF",
		config.Logger(jaeger.StdLogger),
	)
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	opentracing.SetGlobalTracer(tracer)
	defer closer.Close()

	var db *sql.DB
	{
		var err error
		// Connect to the "ordersdb" database
		db, err = sql.Open("mysql", "root@tcp(192.168.10.73:32561)/test")
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}

		//db.Ping()

	}

	// Create Order Service
	var svc order.Service
	{

		repository, err := tidb.New(db, logger, tracer)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = ordersvc.NewService(repository, logger)
		// Add service middleware here
		// Logging middleware
		svc = middleware.LoggingMiddleware(logger)(svc)

	}

	// Create Go kit endpoints for the Order Service
	// Then decorates with endpoint middlewares
	var endpoints transport.Endpoints
	{
		endpoints = transport.MakeEndpoints(svc)

		// add middleware
		endpoints = transport.Endpoints{
			Create:       ot.TraceServer(tracer, "Create")(endpoints.Create),
			GetByID:      ot.TraceServer(tracer, "GetByID")(endpoints.GetByID),
			ChangeStatus: ot.TraceServer(tracer, "ChangeStatus")(endpoints.ChangeStatus),
		}

	}

	var h http.Handler
	{
		serverOptions := []kithttp.ServerOption{}
		h = httptransport.NewService(endpoints, serverOptions, tracer, logger)
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		level.Info(logger).Log("transport", "HTTP", "addr", *httpAddr)
		server := &http.Server{
			Addr:    *httpAddr,
			Handler: h,
		}
		errs <- server.ListenAndServe()
	}()

	level.Error(logger).Log("exit", <-errs)
}
