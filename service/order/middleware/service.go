package middleware

import "github.com/ntels/bsf/service/order"

// Middleware describes a service middleware.
type Middleware func(service order.Service) order.Service

