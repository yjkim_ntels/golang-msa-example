package tidb

import (
	"context"
	"database/sql"
	"errors"
	"strconv"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"

	"github.com/ntels/bsf/service/order"
)

var (
	ErrRepository = errors.New("unable to handle request")
)

type repository struct {
	db     *sql.DB
	logger log.Logger
	tracer opentracing.Tracer
}

func New(db *sql.DB, logger log.Logger, tracer opentracing.Tracer) (order.Repository, error) {
	return &repository{
		db:     db,
		logger: log.With(logger, "repository", "tidb"),
		tracer: tracer,
	}, nil
}

func (repo *repository) CreateOrder(ctx context.Context, order order.Order) (order.Order, error) {
	//logger := log.With(repo.logger, "method", "Create")
	//level.Info(logger).Log("log test in repo ", "value")

	tx, err := repo.db.Begin()
	if err != nil {
		return order, err
	}
	defer tx.Commit()

	{
		orderStmt, err := tx.Prepare(`
			INSERT INTO orders (customerid,useyn,createOn, restaurantid)
			  VALUES(?,?,?,?)
		`)
		if err != nil {
			tx.Rollback()
			return order, err
		}
		defer orderStmt.Close()

		res, err := orderStmt.Exec(order.CustomerID, order.UseYn, order.CreatedOn, order.RestaurantID)
		if err != nil {
			tx.Rollback()
			return order, err
		}

		id, _ := res.LastInsertId()
		order.ID = strconv.FormatInt(id, 10)

		itemStmt, err := tx.Prepare(`
				INSERT INTO orderitems (order_id,product_code, name, unitprice, quantity)
					VALUES (?,?,?,?,?)
			`)
		if err != nil {
			tx.Rollback()
			return order, err
		}
		defer itemStmt.Close()

		for _, v := range order.OrderItems {
			if _, err := itemStmt.Exec(order.ID, v.ProductCode, v.Name, v.UnitPrice, v.Quantity); err != nil {
				tx.Rollback()
				return order, err
			}
		}

	}

	return order, tx.Commit()
}

// ChangeOrderStatus changes the order status
func (repo *repository) ChangeOrderStatus(ctx context.Context, orderId string, status string) error {
	sql := `
UPDATE orders
SET status=
WHERE id= `

	_, err := repo.db.ExecContext(ctx, sql, orderId, status)
	if err != nil {
		return err
	}
	return nil
}

// GetOrderByID query the order by given id
func (repo *repository) GetOrderByID(ctx context.Context, id string) (order.Order, error) {
	logger := log.With(repo.logger, "method", "Create")
	//level.Info(logger).Log("log test in repo ", "value")
	var orderRow = order.Order{}

	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := repo.tracer.StartSpan("SQL SELECT", opentracing.ChildOf(span.Context()))

		tags.SpanKindRPCClient.Set(span)
		tags.PeerService.Set(span, "mysql")
		// #nosec
		span.SetTag("sql.query", "SELECT id, customerid, createon, restaurantid FROM orders WHERE id ="+id)
		//logger.Info(ctx).Log()
		//level.Info(logger).Log("log test in repo ", "value")
		logger.Log("1", "2")
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	if err := repo.db.QueryRowContext(ctx,
		"SELECT id, customerid, createon, restaurantid FROM orders WHERE id = ?",
		id).
		Scan(
			&orderRow.ID, &orderRow.CustomerID, &orderRow.CreatedOn, &orderRow.RestaurantID,
		); err != nil {
		level.Error(repo.logger).Log("err", err.Error())
		return orderRow, err
	}

	// ToDo: Query order items from orderitems table
	return orderRow, nil
}

func (repo *repository) Close() error {
	return repo.db.Close()
}
